# Docker

## prerequisites

dialog package should be installed in the docker host.

For Ubuntu:

apt-get install -y dialog

For CentOS:

yum install dialog

## Getting started

* Download the script docker-build.sh

* Make it executable by # chmod +x docker-build.sh

* Run the script, # sh docker-build.sh

* Once the script execution is completed, run # docker ps to view the running containers 'wordpress' and 'database'.
